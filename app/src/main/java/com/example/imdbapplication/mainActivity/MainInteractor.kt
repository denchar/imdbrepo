package com.example.imdbapplication.mainActivity

import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject


interface MainInteractor{
    val dataFromServer: BehaviorSubject<List<MovieModel>>
    fun getLiveDataRequest(pageNumber: Int)
    fun gettingListFromString(genreIds: String): List<Int>
    fun writingStringFromList(list: List<Int>): String
    fun onCleared()
}
class MainInteractorImpl:MainInteractor, KoinComponent {

    private val repository:MainRepository  by inject()

    override val dataFromServer: BehaviorSubject<List<MovieModel>>
        get() = repository.dataFromServer

    override fun getLiveDataRequest(pageNumber: Int) {
        repository.getLiveDataRequest(pageNumber)
    }

    override fun gettingListFromString(genreIds: String): List<Int> {
        return repository.gettingListFromString(genreIds)
    }

    override fun writingStringFromList(list: List<Int>): String {
        return repository.writingStringFromList(list)
    }

    override fun onCleared() {
        repository.onCleared()
    }
}