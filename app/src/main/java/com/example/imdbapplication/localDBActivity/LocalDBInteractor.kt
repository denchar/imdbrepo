package com.example.imdbapplication.localDBActivity

import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

interface LocalDBInteractor {
    val dataFromDB: BehaviorSubject<List<MovieModel>>
    fun onCleared()
}

class LocalDBInteractorImpl : LocalDBInteractor, KoinComponent {
    private val repository: LocalDBRepository by inject()
    override val dataFromDB: BehaviorSubject<List<MovieModel>>
        get() = repository.dataFromDB

    override fun onCleared() {
        repository.onCleared()
    }

}