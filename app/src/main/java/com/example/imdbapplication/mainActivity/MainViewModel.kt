package com.example.imdbapplication.mainActivity


import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.koin.core.KoinComponent
import org.koin.core.get

class MainViewModel(application: Application) :
    AndroidViewModel(application),KoinComponent {

    private val interactor: MainInteractor = get()
    private var pageNumber = 0
    private var currentPopularData = MutableLiveData<List<MovieModel>>()
    private val compositeDisposable = CompositeDisposable()
    var isLoading = ObservableField<Boolean>(false)
    var haveConnection = ObservableField<Boolean>()

    init {
        isLoading.set(true)

        //setupBindings()
        setupData()
        loadData()
    }

    override fun onCleared() {
        interactor.onCleared()
        compositeDisposable.clear()
        super.onCleared()
    }

    fun getMainViewModelData(): MutableLiveData<List<MovieModel>> {
        return currentPopularData
    }

    fun loadData() {
        Log.d("MainViewModel", "Loading data")
        pageNumber++
        interactor.getLiveDataRequest(pageNumber)
    }

    private fun setupData() {
        interactor
            .dataFromServer
            .subscribeBy(
                onNext = {
                    Log.d("MainViewModel", "Got new data from Server, emmit currentPopularData")
                    currentPopularData.value = it
                }).addTo(compositeDisposable)
    }
}