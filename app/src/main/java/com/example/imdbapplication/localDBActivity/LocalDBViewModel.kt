package com.example.imdbapplication.localDBActivity

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.koin.core.KoinComponent
import org.koin.core.get

class LocalDBViewModel(application: Application) :
    AndroidViewModel(application),KoinComponent {
    private val interactor: LocalDBInteractor = get()
    private var currentPopularData = MutableLiveData<List<MovieModel>>()
    private val compositeDisposable = CompositeDisposable()
    var isLoading = ObservableField<Boolean>(false)
    var isDBListEmpty = ObservableField<Boolean>(false)

    init {
        isLoading.set(true)
        setupBindings()
    }

    private fun setupBindings() {
        interactor
            .dataFromDB
            .subscribeBy(
                onNext = {
                    currentPopularData.value = it
                })
            .addTo(compositeDisposable)
    }

    fun getMainViewModelData(): MutableLiveData<List<MovieModel>> {
        return currentPopularData
    }

    override fun onCleared() {
        interactor.onCleared()
        compositeDisposable.clear()
        super.onCleared()
    }
}