package com.example.imdbapplication.detailActivity

import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel

interface DetailRepository {
    fun getVideos(id: Int)
    fun getReviews(id: Int)
    fun getImages(id: Int)
    fun addItemInDatabase()
    fun removeItemFromDatabase()
    fun getCurrentVideoData()  : MutableLiveData<ArrayList<MovieVideoModel.Result>>
    fun getCurrentReviewData():MutableLiveData<ArrayList<MovieReviewModel.Result>>
    fun getCurrentImagesData():MutableLiveData<ArrayList<MovieImagesModel.Backdrop>>
    fun onCleared()
}