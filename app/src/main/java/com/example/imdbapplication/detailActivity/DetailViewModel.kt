package com.example.imdbapplication.detailActivity

import android.app.Application
import android.content.Context
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.CacheClass
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import com.squareup.picasso.Picasso
import org.jetbrains.annotations.NotNull
import org.koin.core.KoinComponent
import org.koin.core.get


class DetailViewModel(application: Application) :
    AndroidViewModel(application), KoinComponent {

    private val detailInteractor: DetailInteractor = get()
    private var context: Context = getApplication()
    private var currentVideoData = MutableLiveData<ArrayList<MovieVideoModel.Result>>()
    private var currentReviewData = MutableLiveData<ArrayList<MovieReviewModel.Result>>()
    private var currentImagesData = MutableLiveData<ArrayList<MovieImagesModel.Backdrop>>()
    val inDatabase = ObservableField<Boolean>(false)


    val id = ObservableField<Int>(CacheClass.movieModel.id)
    val voteCount = ObservableField<Int>(CacheClass.movieModel.vote_count)
    val video = ObservableField<Boolean>(CacheClass.movieModel.video)
    val voteAverage = ObservableField<String>(
        context.resources.getString(R.string.vote_average)
                + CacheClass.movieModel.vote_average
                + context.resources.getString(
            R.string.from_ten
        )
    )
    val popularity =
        ObservableField<Float>(CacheClass.movieModel.popularity)
    val backdropPath =
        ObservableField<String>(CacheClass.movieModel.backdrop_path)
    val title = ObservableField<String>(CacheClass.movieModel.title)
    val release =
        ObservableField<String>(CacheClass.movieModel.release_date)
    val overview = ObservableField<String>(CacheClass.movieModel.overview)
    val posterPath =
        ObservableField<String>(
            Values.BASE_IMAGE_URL
                    + Values.RECOMENDED_IMAGE_SIZE
                    + CacheClass.movieModel.poster_path
        )
    val genreIds =
        ObservableField<String>(Values.getGenres(CacheClass.movieModel.genre_ids))

    object MyDataBinding {
        @BindingAdapter("app:imageUrl")
        @JvmStatic
        fun loadImage(@NotNull view: ImageView, url: String) {
            Picasso.get().load(url).into(view)
        }
    }

    init {
        currentVideoData = detailInteractor.getCurrentVideoData()
        currentReviewData = detailInteractor.getCurrentReviewData()
        currentImagesData = detailInteractor.getCurrentImagesData()
        detailInteractor.getVideos(id.get()!!)
        detailInteractor.getReviews(id.get()!!)
        detailInteractor.getImages(id.get()!!)
    }

    fun getMovieVideos() = currentVideoData

    fun getMovieReviews() = currentReviewData

    fun getMovieImage() = currentImagesData

    fun addItemInDatabase(){
        detailInteractor.addItemInDatabase()
    }
    fun removeItemFromDatabase(){
        detailInteractor.removeItemFromDatabase()
    }

    override fun onCleared() {
        detailInteractor.onCleared()
        super.onCleared()
    }

}