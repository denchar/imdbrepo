package com.example.imdbapplication.mainActivity


import android.annotation.SuppressLint
import android.util.Log
import androidx.room.TypeConverter
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.database.MyCinemaDao
import com.example.imdbapplication.database.MyCinemaModel
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.movieModels.MovieResponceModel
import com.example.imdbapplication.retrofit.ApiClient
import com.example.imdbapplication.retrofit.ApiInterface
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get
import java.util.*
import kotlin.collections.ArrayList

open class MainRepositoryImpl : MainRepository, KoinComponent {
    private val cinemaDB: MyCinemaDao = get()
    private val api: ApiInterface = get()

    private val tag = "MainRepository"

    // private lateinit var api: ApiInterface
    private val compositeDisposable = CompositeDisposable()

    //val dataFromDB: BehaviorSubject<List<String>> = BehaviorSubject.create()
    override val dataFromServer: BehaviorSubject<List<MovieModel>> = BehaviorSubject.create()

    init {
        Log.d("MainRepository", "Main repository initialization")
        addAllCinemaInListFromRoomDB()
    }

    @SuppressLint("CheckResult")
    private fun addAllCinemaInListFromRoomDB() {
        cinemaDB.getAllCinemasFlowable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.isEmpty()) {
                    Log.d(tag, "listfromdb empty")
                }
                // Values.idListFromDatabase.onNext(convertListMyCinemaModelInMovieModel(it))
                Values.idListFromDatabase.value = convertListMyCinemaModelInMovieModel(it)
            }
    }

    private fun convertListMyCinemaModelInMovieModel(list: List<MyCinemaModel>): List<String> {
        val listMovieModel = ArrayList<String>()
        Log.d("MainRepository", "list db size = " + list.size)
        for (item in list) {
            listMovieModel.add(item.cinema_id.toString())
            Log.d(
                "MainRepository",
                "cinema id = " + item.cinema_id + ", cinema title = " + item.title
            )
        }
        return listMovieModel
    }

    override fun getLiveDataRequest(pageNumber: Int) {
        Log.d("MainRepository", "Get data from server request")

        //api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)

        compositeDisposable.add(
            api.getPopular(Values.API_KEY, pageNumber)
                // .map { result -> writeCinemaInRoomDB(result) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { addRequestResultInDataArray(it) },
                    {
                        it.printStackTrace()
                        sendErrResult()
                    },
                    {
                        println("Done!")
                        Log.d("MainRepository", "Done")
                    }
                )
        )

    }

    private fun sendErrResult() {
        println("Error")
    }

    @SuppressLint("CheckResult")
    private fun addRequestResultInDataArray(result: MovieResponceModel) {
        dataFromServer.onNext(result.results)

    }


    @TypeConverter
    override fun gettingListFromString(genreIds: String): List<Int> {
        val list = ArrayList<Int>()

        val array = genreIds.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        for (s in array) {
            if (s.isNotEmpty()) {
                list.add(Integer.parseInt(s))
            }
        }
        return list
    }

    @TypeConverter
    override fun writingStringFromList(list: List<Int>): String {
        var genreIds = ""
        for (i in list) {
            genreIds += ",$i"
        }
        return genreIds
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}