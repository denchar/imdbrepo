package com.example.imdbapplication.database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "mycinematable")
class MyCinemaModel(
    @ColumnInfo(name = "cinema_id") var cinema_id: Int,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "poster_path") var poster_path: String,
    @ColumnInfo(name = "overview") var overview: String,
    @ColumnInfo(name = "release_date") var release_date: String,
    @ColumnInfo(name = "genre_ids") var genre_ids: String

    /*
    @ColumnInfo(name = "video") var video: Boolean,
    @ColumnInfo(name = "vote_average") var vote_average: Float,
    @ColumnInfo(name = "vote_count") var vote_count: Int,
    @ColumnInfo(name = "popularity") var popularity: Float,
    @ColumnInfo(name = "original_language") var original_language: String,
    @ColumnInfo(name = "original_title") var original_title: String,
    @ColumnInfo(name = "backdrop_path") var backdrop_path: String,
    @ColumnInfo(name = "adult") var adult: Boolean,
    @ColumnInfo(name = "overview") var overview: String,
    @ColumnInfo(name = "release_date") var release_date: String,
    @ColumnInfo(name = "genre_ids") var genre_ids: ArrayList<Int>
*/
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

}