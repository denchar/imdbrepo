package com.example.imdbapplication.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "mycinemagenretable")
class MyCinemaGenresModel(
    @ColumnInfo(name = "cinema_id") var cinema_id: Int,
    @ColumnInfo(name = "genre_id") var genre_id: Int) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}