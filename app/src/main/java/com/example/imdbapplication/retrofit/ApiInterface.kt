package com.example.imdbapplication.retrofit

import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieResponceModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("movie/popular?append_to_response=genre_ids")
    fun getPopular(@Query("api_key") key: String, @Query("page") page: Int): Observable<MovieResponceModel>

    //  "movie/popular?language=en-US&append_to_response=genre_ids"
    @GET("movie/top_rated?language=en-US")
    fun getTopRaited(@Query("api_key") key: String): Observable<MovieResponceModel>

    @GET("movie/{id}/videos?language=en-US")
    fun getMovieVideos(@Path("id") id: Int, @Query("api_key") key: String): Observable<MovieVideoModel>

    @GET("movie/{id}/reviews?page=1")
    fun getMovieReviews(@Path("id") id: Int, @Query("api_key") key: String): Observable<MovieReviewModel>

    @GET("search/movie?include_adult=false")
    fun searchMoviesPaged(
        @Query("query") title: String,
        @Query("page") page: Int,
        @Query("api_key") key: String
    ): Observable<MovieResponceModel>

    @GET("genre/{genre_name}/movies?")
    fun getMovieByGenre(
        @Path("genre_name") genreName: String,
        @Query("api_key") key: String,
        @Query("page") page: Int
    ): Observable<MovieResponceModel>

    @GET("movie/{id}/images?include_image_language=en%2Cnull&language=en-US")
    fun getMovieImages(
        @Path("id") id: Int,
        @Query("api_key") key: String
    ): Observable<MovieImagesModel>
    /*

    https://api.themoviedb.org/3/movie/495764/images?api_key=35ffe1c2c8248e38e4cbb1a187bee8e8&language=en-US&include_image_language=en%2Cnull

    https://api.themoviedb.org/3/genre/28-action/movies?api_key=35ffe1c2c8248e38e4cbb1a187bee8e8
https://api.themoviedb.org/3/search/movie?include_adult=false&page=1&query=star&language=en-US&api_key=35ffe1c2c8248e38e4cbb1a187bee8e8
    https://api.themoviedb.org/3/search/keyword?page=1&query=star&append_to_response=keywords,credits,images&api_key=35ffe1c2c8248e38e4cbb1a187bee8e8
https://api.themoviedb.org/3/movie/490132?api_key=35ffe1c2c8248e38e4cbb1a187bee8e8&language=en-US
     @GET("/3/movie/{id}?append_to_response=keywords,credits,images")
  fun getMovieById(@Path("id") id: Long): Single<MovieDetailedNM>

  @GET("/3/tv/{id}?append_to_response=keywords,credits,images")
  fun getShowById(@Path("id") id: Long): Single<ShowDetailedNM>

  @GET("/3/movie/popular")
  fun getPopularMovies(@Query("page") page: Int): Single<PagedMovieApiListNM<MovieBriefNM>>

  @GET("/3/discover/movie")
  fun filterMoviesPaged(
    @QueryMap options: Map<String, String>,
    @Query("page") page: Int
  ): Single<PagedMovieApiListNM<MovieBriefNM>>

  @GET("/3/search/movie?append_to_response=keywords,credits,images")
  fun searchMoviesPaged(
    @Query("query") title: String,
    @Query("page") page: Int
  ): Single<PagedMovieApiListNM<MovieBriefNM>>

  @GET("/3/search/tv?append_to_response=keywords,credits,images")
  fun searchShowsPaged(
    @Query("query") title: String,
    @Query("page") page: Int
  ): Single<PagedMovieApiListNM<ShowBriefNM>>

  @GET("/3/search/keyword")
  fun searchKeywordsPaged(
    @Query("query") name: String,
    @Query("page") page: Int
  ): Single<PagedMovieApiListNM<KeywordNM>>

  @GET("/3/search/person")
  fun searchCastPaged(
    @Query("query") name: String,
    @Query("page") page: Int
  ): Single<PagedMovieApiListNM<CastNM>>

  @GET("/3/genre/movie/list")
  fun getGenres(): Single<List<GenreNM>>
    */
}