package com.example.imdbapplication.movieModels

class MovieImagesModel(
    val id: Int,
    val backdrops: List<Backdrop>,
    val posters: List<Poster>
) {
    class Backdrop(
        val aspect_ratio: Float,
        val file_path: String,
        val height: Int,
        val iso_639_1: String?,
        val vote_average: String,
        val vote_count: Int,
        val width: Int
    )

    class Poster(
        val aspect_ratio: Float,
        val file_path: String,
        val height: Int,
        val iso_639_1: String?,
        val vote_average: String,
        val vote_count: Int,
        val width: Int
    )
}