package com.example.imdbapplication.detailActivity

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.databinding.ActivityDetailBinding
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity(), DetailTrailerAdapter.ViewHolderClickListener {

    private lateinit var activityDetailBinding: ActivityDetailBinding
    private val detailViewModel: DetailViewModel by viewModel()
    private val detailTrailerAdapter: DetailTrailerAdapter by inject()
    private val detailReviewAdapter: DetailReviewAdapter by inject()
    private val detailImageAdapter: DetailImageAdapter by inject()
    private val tag = "DetailActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        activityDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        activityDetailBinding.viewModel = detailViewModel
        activityDetailBinding.executePendingBindings()

        detailTrailerAdapter.setListener(this)
        trailers_recycler_view.adapter = detailTrailerAdapter
        reviews_recycler_view.adapter = detailReviewAdapter
        images_recycler_view.adapter = detailImageAdapter

        detailViewModel.getMovieVideos()
            .observe(this, Observer<ArrayList<MovieVideoModel.Result>> { list ->
                detailTrailerAdapter.addListInData(list)
            })
        detailViewModel.getMovieReviews()
            .observe(this, Observer<ArrayList<MovieReviewModel.Result>> { list ->
                detailReviewAdapter.addListInData(list)
            })
        detailViewModel.getMovieImage()
            .observe(this, Observer <ArrayList<MovieImagesModel.Backdrop>>{ list ->
                detailImageAdapter.addListInData(list)
            })

        detail_add_to_database_image_button.setOnClickListener { detailViewModel.addItemInDatabase() }
        detail_remove_from_database_image_button.setOnClickListener { detailViewModel.removeItemFromDatabase() }
        visibleInvisibleDatabaseButtons()
    }

    override fun onViewHolderClick(result: MovieVideoModel.Result) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(this.resources.getString(R.string.youtube_url) + result.key)
            )
        )
    }

    private fun visibleInvisibleDatabaseButtons() {
        if (Values.idListFromDatabase.value != null) {
            Values.idListFromDatabase.observe(this, Observer<List<String>> {
                for (item in it){
                    if (item == detailViewModel.id.get().toString()){
                        Log.d(tag, "in database = ${detailViewModel.id.get().toString()}")
                    }
                }

                if (Values.idListFromDatabase.value?.contains(detailViewModel.id.get().toString())!!) {
                    detailViewModel.inDatabase.set(true)
                    Log.d(tag, "inDatabase = ${detailViewModel.inDatabase.get()}")
                } else {
                    detailViewModel.inDatabase.set(false)
                    Log.d(tag, "inDatabase = ${detailViewModel.inDatabase.get()}")
                }
            })

        }
    }

    companion object {
        fun startMovieDetailActivity(context: Context) {
            context.startActivity(Intent(context, DetailActivity::class.java))
        }
    }
}
