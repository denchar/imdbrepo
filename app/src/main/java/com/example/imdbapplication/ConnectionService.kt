package com.example.imdbapplication

import android.content.Context
import android.net.ConnectivityManager
import androidx.databinding.ObservableField

class ConnectionService(private val context: Context) {

    private val connect = ObservableField<Boolean>()
    init {
        isConnect(context)
    }

    private fun isConnect(context: Context) {
        var network = false
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        if (netInfo != null && netInfo.isConnected) {
            network = true
        }
        connect.set(network)
    }
    fun getConnectStatus():ObservableField<Boolean> {
        return connect
    }
}