package com.example.imdbapplication.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Flowable

@Dao
interface MyCinemaDao {
    @Insert
    fun addCinema(user: MyCinemaModel)

    @Query("SELECT * from mycinematable")
    fun getAllCinemas(): List<MyCinemaModel>

    @Query("SELECT * from mycinematable")
    fun getAllCinemasFlowable(): Flowable<List<MyCinemaModel>>

    @Query("SELECT * from mycinematable WHERE cinema_id=:cinema_id")
    fun getCinemabyId(cinema_id:Int): List<MyCinemaModel>

    @Query("DELETE from mycinematable")
    fun deleteAllCinemas()

    @Query("DELETE from mycinematable WHERE cinema_id=:cinema_id")
    fun deleteCinemaById(cinema_id: Int)

    @Insert
    fun addGenres(genres:MyCinemaGenresModel)

    @Query("SELECT * from mycinemagenretable")
    fun getAllGenres():List<MyCinemaGenresModel>

    @Query("SELECT * from mycinemagenretable WHERE genre_id=:id")
    fun getGenresById(id:Int):List<MyCinemaGenresModel>

    @Query("DELETE from mycinemagenretable")
    fun deleteAllGenres()
}