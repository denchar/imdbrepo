package com.example.imdbapplication.mainActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_layout.view.*

class MainRecyclerViewAdapter : RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder>() {

    private var data = ArrayList<MovieModel>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener
    private lateinit var onLoadMoreListener: OnLoadMoreCinemaInAdapterListener
    private lateinit var isLoading: ObservableField<Boolean>


    fun setIsLoading(isLoading: ObservableField<Boolean>) {
        this.isLoading = isLoading
    }

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreCinemaInAdapterListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    fun addListInData(list: List<MovieModel>) {
        data.addAll(list)
        notifyDataSetChanged()
    }

    fun getItemViewPosition(position: Int): MovieModel {
        return data[position]
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.item_movie_layout, p0, false),
            viewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.title.text = data[p1].title
        val url = Values.BASE_IMAGE_URL + Values.RECOMENDED_IMAGE_SIZE + data[p1].poster_path
        Picasso.get().load(url).into(p0.poster)
        p0.bind(data[p1])
        if (!isLoading.get()!! && p0.adapterPosition == itemCount - 1) {
            onLoadMoreListener.onLoadMoreCinemaInAdapter()
        }
    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(item: MovieModel)
    }

    interface OnLoadMoreCinemaInAdapterListener {
        fun onLoadMoreCinemaInAdapter()
    }

    class ViewHolder(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var poster: ImageView = view.poster_image_view
        var title: TextView = view.movie_title_text_view
        lateinit var movieModel: MovieModel

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(movieModel)
            }
        }

        fun bind(movieModel: MovieModel) {
            this.movieModel = movieModel
        }

    }
}