package com.example.imdbapplication.searchActivity


import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.R
import kotlinx.android.synthetic.main.item_genre.view.*


class GenreRecyclerViewAdapter : RecyclerView.Adapter<GenreRecyclerViewAdapter.ViewHolder>() {

    private val tag = "GenreAdapter"
    private val genreList: List<GenreCheckBox> = listOf(
        GenreCheckBox("Action ", false),
        GenreCheckBox("Adventure ", false),
        GenreCheckBox("Animation ", false),
        GenreCheckBox("Comedy ", false),
        GenreCheckBox("Crime ", false),
        GenreCheckBox("Documentary ", false),
        GenreCheckBox("Drama ", false),
        GenreCheckBox("Family ", false),
        GenreCheckBox("Fantasy ", false),
        GenreCheckBox("History ", false),
        GenreCheckBox("Horror ", false),
        GenreCheckBox("Music ", false),
        GenreCheckBox("Mystery ", false),
        GenreCheckBox("Romance ", false),
        GenreCheckBox("Science Fiction ", false),
        GenreCheckBox("TV Movie ", false),
        GenreCheckBox("Thriller ", false),
        GenreCheckBox("War ", false),
        GenreCheckBox("Western ", false)
    )
    private lateinit var genreViewHolderClickListener: GenreViewHolderClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_genre, parent, false),
            genreViewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        return genreList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.genreCheckBox.text = genreList[position].genreName
        if (!genreList[position].check) {
            Log.d(
                tag,
                "position = $position, genreList.position.check = ${genreList[position].check.toString()}"
            )
            holder.genreCheckBox.setTextColor(Color.BLACK)
        } else {
            Log.d(
                tag,
                "position = $position, genreList.position.check = ${genreList[position].check.toString()}"
            )
            holder.genreCheckBox.setTextColor(Color.RED)
        }

        holder.bind(genreList[position])
    }

    fun setGenreViewHolderClickListener(genreViewHolderClickListener: GenreViewHolderClickListener) {
        this.genreViewHolderClickListener = genreViewHolderClickListener
    }

    interface GenreViewHolderClickListener {
        fun onGenreViewHolderClick(item: GenreCheckBox, position: Int)
    }

    fun setCheckAndUncheckForItems(position: Int): Boolean {
        Log.d(tag, "position = $position, genreList.size = ${genreList.size}")
        var itemValueCheck = false
        for (i in genreList.indices) {
            if (i == position) {
                genreList[i].check = !genreList[i].check
                itemValueCheck = genreList[i].check
            } else {
                genreList[i].check = false
            }
        }
        return itemValueCheck
    }

    fun setUnCheckForItems() {
        for (item in genreList) {
            item.check = false
        }
    }

    class ViewHolder(view: View, genreViewHolderClickListener: GenreViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var genreCheckBox: TextView = view.genre_text_view
        lateinit var genre: GenreCheckBox

        init {
            itemView.setOnClickListener {
                genreViewHolderClickListener.onGenreViewHolderClick(genre, adapterPosition)
            }
        }

        fun bind(genre: GenreCheckBox) {
            this.genre = genre
        }
    }
}