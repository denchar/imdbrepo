package com.example.imdbapplication.koin

import androidx.room.Room
import com.example.imdbapplication.ConnectionService
import com.example.imdbapplication.database.MyRoomCinemaDatabase
import com.example.imdbapplication.detailActivity.*
import com.example.imdbapplication.localDBActivity.*
import com.example.imdbapplication.mainActivity.*
import com.example.imdbapplication.retrofit.ApiClient
import com.example.imdbapplication.retrofit.ApiInterface
import com.example.imdbapplication.searchActivity.*
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DependencyModule {
    val detailModule = module {
        factory { DetailRepositoryImpl() as DetailRepository }
        factory { DetailInteractorImpl() as DetailInteractor}
        viewModel { DetailViewModel(get()) }
        factory { DetailTrailerAdapter() }
        factory { DetailReviewAdapter() }
        factory { DetailImageAdapter() }
    }
    val mainModule = module {
        factory { MainRecyclerViewAdapter() }
        viewModel { MainViewModel(get()) }
        single {
            Room.databaseBuilder(
                androidApplication(),
                MyRoomCinemaDatabase::class.java,
                MyRoomCinemaDatabase.DB_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }
        single { get<MyRoomCinemaDatabase>().myDao() }
        single {
            ApiClient.getApiRetrofitClient()?.create(ApiInterface::class.java)
        }
        factory { MainRepositoryImpl()  as MainRepository}
        factory { MainInteractorImpl() as MainInteractor }
        single { ConnectionService(get()) }
    }
    val searchModule = module {
        viewModel { SearchViewModel(get()) }
        factory { SearchRepositoryImpl() as SearchRepository }
        factory { SearchInteractorImpl() as SearchInteractor}
        factory { SearchRecyclerViewAdapter() }
        factory { GenreRecyclerViewAdapter() }
    }
    val localDBModule = module {
        factory { LocalDBAdapter() }
        viewModel { LocalDBViewModel(get()) }
        factory { LocalDBRepositoryImpl() as LocalDBRepository }
        factory { LocalDBInteractorImpl() as LocalDBInteractor}

    }
}