package com.example.imdbapplication.searchActivity

import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject

interface SearchRepository {
    val dataFromServerBehaviorSubject: BehaviorSubject<List<MovieModel>>
    fun getDataFromServer(): MutableLiveData<List<MovieModel>>
    fun searchCinemaByKeyword(searchKeyword: String, pageNumber: Int)
    fun searchCinemaByGenre(searchKeyword: String, pageNumber: Int)
    fun onCleared()
}