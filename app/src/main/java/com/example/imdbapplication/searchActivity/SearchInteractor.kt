package com.example.imdbapplication.searchActivity

import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

interface SearchInteractor {
    val dataFromServerBehaviorSubject: BehaviorSubject<List<MovieModel>>
    fun getDataFromServer(): MutableLiveData<List<MovieModel>>
    fun searchCinemaByKeyword(searchKeyword: String, pageNumber: Int)
    fun searchCinemaByGenre(searchKeyword: String, pageNumber: Int)
    fun onCleared()
}

class SearchInteractorImpl : SearchInteractor, KoinComponent {

    private val repository: SearchRepository by inject()
    override val dataFromServerBehaviorSubject: BehaviorSubject<List<MovieModel>>
        get() = repository.dataFromServerBehaviorSubject

    override fun getDataFromServer(): MutableLiveData<List<MovieModel>> {
        return repository.getDataFromServer()
    }

    override fun searchCinemaByKeyword(searchKeyword: String, pageNumber: Int) {
        repository.searchCinemaByKeyword(searchKeyword, pageNumber)
    }

    override fun searchCinemaByGenre(searchKeyword: String, pageNumber: Int) {
        repository.searchCinemaByGenre(searchKeyword, pageNumber)
    }

    override fun onCleared() {
        repository.onCleared()
    }

}