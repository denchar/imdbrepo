package com.example.imdbapplication.searchActivity


import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.movieModels.MovieResponceModel
import com.example.imdbapplication.retrofit.ApiClient
import com.example.imdbapplication.retrofit.ApiInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get

class SearchRepositoryImpl : SearchRepository, KoinComponent{
    private val api:ApiInterface = get()

    private val tag = "SearchRepository"
    //private lateinit var api: ApiInterface
    private val compositeDisposable = CompositeDisposable()
    private val dataFromServer: MutableLiveData<List<MovieModel>> = MutableLiveData()
    override val dataFromServerBehaviorSubject: BehaviorSubject<List<MovieModel>> = BehaviorSubject.create()
    private var totalPages = 1

    override fun getDataFromServer(): MutableLiveData<List<MovieModel>> {
        return dataFromServer
    }

    override fun searchCinemaByKeyword(searchKeyword: String, pageNumber: Int) {
        println("SearchRepository = $searchKeyword")
        Log.d(tag, searchKeyword)
        if (pageNumber <= totalPages) {
           // api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)
            compositeDisposable.addAll(
                api.searchMoviesPaged(searchKeyword, pageNumber, Values.API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        printResult(it)
                    })
        } else {
            Log.d(tag, "error!  totalPages = $totalPages")
        }
    }

    private fun printResult(result: MovieResponceModel) {
        totalPages = result.total_pages
        if (totalPages == 0) {
            totalPages = 1
        }
        dataFromServer.value = result.results
        dataFromServerBehaviorSubject.onNext(result.results)
    }

    override fun searchCinemaByGenre(searchKeyword: String, pageNumber: Int) {
      //  api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)
        compositeDisposable.addAll(
            api.getMovieByGenre(searchKeyword, Values.API_KEY, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    printResult(it)
                }
        )
    }
    // Observable.fromArray(field).
    /*  editText.observeChanges()
           .debounce(500, TimeUnit.MILLISECONDS)
           .map(String::toLowerCase)
           .flatMap(this::findPerson)
           .subscribeOn(Schedulers.io())
           .observeOn(AndroidSchedulers.mainThread())
           .subscribe(person -> {}, throwable -> {})
       */

    override fun onCleared() {
        compositeDisposable.clear()
    }
}