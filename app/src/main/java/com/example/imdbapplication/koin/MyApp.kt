package com.example.imdbapplication.koin

import android.app.Application
import com.example.imdbapplication.koin.DependencyModule.detailModule
import com.example.imdbapplication.koin.DependencyModule.localDBModule
import com.example.imdbapplication.koin.DependencyModule.mainModule
import com.example.imdbapplication.koin.DependencyModule.searchModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MyApp)
            modules(listOf(detailModule,mainModule,searchModule,localDBModule))
        }
    }
}