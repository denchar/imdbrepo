package com.example.imdbapplication.localDBActivity

import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject

interface LocalDBRepository {
    val dataFromDB: BehaviorSubject<List<MovieModel>>
    fun onCleared()
}