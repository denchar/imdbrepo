package com.example.imdbapplication.localDBActivity

import android.annotation.SuppressLint
import androidx.room.TypeConverter
import com.example.imdbapplication.database.MyCinemaDao
import com.example.imdbapplication.database.MyCinemaModel
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get

class LocalDBRepositoryImpl():LocalDBRepository, KoinComponent {

    private val database: MyCinemaDao = get()

    private val compositeDisposable = CompositeDisposable()
    override val dataFromDB: BehaviorSubject<List<MovieModel>> = BehaviorSubject.create()

    init {
        addAllCinemaInListFromRoomDB()
    }

    @SuppressLint("CheckResult")
    private fun addAllCinemaInListFromRoomDB() {
        database.getAllCinemasFlowable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                dataFromDB.onNext(convertListMyCinemaModelInMovieModel(it))
            }
    }

    private fun convertListMyCinemaModelInMovieModel(list: List<MyCinemaModel>): List<MovieModel> {
        val listMovieModel = ArrayList<MovieModel>()
        for (item in list) {
            val movieModel = MovieModel(
                1,
                item.cinema_id,
                false,
                item.cinema_id.toFloat(),
                item.title,
                item.cinema_id.toFloat(),
                item.poster_path,
                item.title,
                item.title,
                item.poster_path,
                false,
                item.overview,
                item.release_date,
                gettingListFromString(item.genre_ids) as ArrayList<Int>
            )
            listMovieModel.add(movieModel)
        }
        return listMovieModel
    }

    @TypeConverter
    fun gettingListFromString(genreIds: String): List<Int> {
        val list = ArrayList<Int>()

        val array = genreIds.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        for (s in array) {
            if (s.isNotEmpty()) {
                list.add(Integer.parseInt(s))
            }
        }
        return list
    }


    override fun onCleared() {
        compositeDisposable.clear()
    }
}