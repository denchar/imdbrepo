package com.example.imdbapplication.localDBActivity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.imdbapplication.CacheClass

import com.example.imdbapplication.R
import com.example.imdbapplication.databinding.ActivityLocalDbBinding
import com.example.imdbapplication.detailActivity.DetailActivity
import com.example.imdbapplication.movieModels.MovieModel
import kotlinx.android.synthetic.main.activity_local_db.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class LocalDbActivity : AppCompatActivity(), LocalDBAdapter.ViewHolderClickListener {

    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var activityLocalBinding: ActivityLocalDbBinding
    private val localDBViewModel: LocalDBViewModel by viewModel()
    private val adapter: LocalDBAdapter by inject()
    private val tag = "LocalDbActivity"

    companion object {
        fun startLocalDBActivity(context: Context) {
            context.startActivity(Intent(context, LocalDbActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_local_db)
        activityLocalBinding = DataBindingUtil.setContentView(this, R.layout.activity_local_db)
        activityLocalBinding.viewModel = localDBViewModel
        activityLocalBinding.executePendingBindings()

        adapter.setListener(this)
        local_database_recycler_view.adapter = adapter

        localDBViewModel.getMainViewModelData().observe(this, Observer<List<MovieModel>> {
            localDBViewModel.isLoading.set(false)
            if (it.isNotEmpty()) {
                Log.d(tag, it.size.toString())
                localDBViewModel.isDBListEmpty.set(false)
            } else {
                localDBViewModel.isDBListEmpty.set(true)
                Log.d(tag, "db list is empty")
            }
            adapter.addListInData(it)
        })
    }

    override fun onViewHolderClick(item: MovieModel) {
        CacheClass.movieModel = item
        DetailActivity.startMovieDetailActivity(this)
    }
}
