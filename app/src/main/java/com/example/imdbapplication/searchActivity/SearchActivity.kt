package com.example.imdbapplication.searchActivity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import com.example.imdbapplication.CacheClass
import com.example.imdbapplication.R
import com.example.imdbapplication.databinding.ActivitySearchBinding
import com.example.imdbapplication.detailActivity.DetailActivity
import com.example.imdbapplication.movieModels.MovieModel
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity(), SearchRecyclerViewAdapter.ViewHolderClickListener,
    SearchRecyclerViewAdapter.OnLoadMoreCinemaInAdapterListener,
    SearchViewModel.WhenNeedDataClearListener,
    GenreRecyclerViewAdapter.GenreViewHolderClickListener, SearchViewModel.GenreViewHolderEditTextSearchListener{


    private val tag = "SearchActivity"
    private lateinit var activitySearchBinding: ActivitySearchBinding
    private val searchViewModel: SearchViewModel by viewModel()
    private val adapter: SearchRecyclerViewAdapter by inject()
    private val genreAdapter: GenreRecyclerViewAdapter by inject()

    companion object {
        fun startSearchActivity(context: Context) {
            context.startActivity(Intent(context, SearchActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_search)

        activitySearchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        activitySearchBinding.viewModel = searchViewModel
        activitySearchBinding.executePendingBindings()

        initView()
    }

    private fun initView() {
        searchViewModel.dataClearListener = this
        searchViewModel.genreViewHolderEditTextSearchListener = this
        adapter.setListener(this)
        adapter.setOnLoadMoreListener(this)
        adapter.setIsLoading(searchViewModel.isLoading)
        search_recycler_view.adapter = adapter
        addListInAdapter()

        genreAdapter.setGenreViewHolderClickListener(this)
        genres_recycler_view.adapter = genreAdapter
    }

    override fun onGenreViewHolderClick(item: GenreCheckBox, position: Int) {
        Log.d(tag, "onGenreClick")
        item.check = genreAdapter.setCheckAndUncheckForItems(position)
        genreAdapter.notifyDataSetChanged()
        searchViewModel.searchText.set("")
        searchViewModel.setGenreCheckBox(item)
    }

    override fun onGenreEditTextSearchClick() {
        Log.d(tag, "onGenreEditTextSearchClick")
        genreAdapter.setUnCheckForItems()
        genreAdapter.notifyDataSetChanged()
    }

    override fun onViewHolderClick(item: MovieModel) {
        CacheClass.movieModel = item
        DetailActivity.startMovieDetailActivity(this)
    }

    override fun onLoadMoreCinemaInAdapter() {
        searchViewModel.onLoadMoreCinemaRefuse(searchViewModel.searchText.get().toString())
       // searchViewModel.searchCinemaByKeyword(searchViewModel.searchText.get().toString())
    }

    private fun addListInAdapter() {
        searchViewModel.getSearchViewModelData().observe(this, Observer<List<MovieModel>> { list ->
            Log.d(tag, "set isLoading ")
            searchViewModel.isLoading.set(false)
            Log.d(tag, " list = ${list.size}")
            adapter.addListInData(list)

            list.forEach { println("from search : " + it.title) }
        })
    }

    override fun dataClear() {
        adapter.dataClear()
    }
}
