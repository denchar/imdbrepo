package com.example.imdbapplication.searchActivity

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get
import java.util.concurrent.TimeUnit


class SearchViewModel(application: Application) :
    AndroidViewModel(application), KoinComponent {

    private val interactor: SearchInteractor = get()
    private val tag = "SearchViewModel"
    private var currentGenreValue = ""
    val isLoading = ObservableField<Boolean>(false)
    lateinit var dataClearListener: WhenNeedDataClearListener
    lateinit var genreViewHolderEditTextSearchListener: GenreViewHolderEditTextSearchListener
    private var pageNumber = 0
    lateinit var currentGenreCheckBox: GenreCheckBox
    private val compositeDisposable = CompositeDisposable()
    var currentData = MutableLiveData<List<MovieModel>>()
    val searchText: ObservableField<String> = object : ObservableField<String>("") {
        @SuppressLint("CheckResult")
        override fun set(value: String?) {
            if (value != null && value != "") {
                genreViewHolderEditTextSearchListener.onGenreEditTextSearchClick()
                Observable.fromArray(value)
                    .debounce(500, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .subscribe {
                        Log.d(tag, "set isLoading = true")
                        isLoading.set(true)
                        dataClearListener.dataClear()
                        pageNumber = 0
                        initCurrentGenreCheckBox()
                        searchCinemaByKeyword(it)
                    }
            }
            return super.set(value)
        }
    }

    init {
        initCurrentGenreCheckBox()
        setupData()
    }

    fun initCurrentGenreCheckBox() {
        currentGenreCheckBox = GenreCheckBox("", false)
    }

    fun onLoadMoreCinemaRefuse(value: String) {
        if (value == "") {
            setGenreCheckBox(currentGenreCheckBox)
        } else {
            searchCinemaByKeyword(value)
        }
    }

    fun searchCinemaByKeyword(value: String?) {
        Log.d(tag, " value = $value")
        if (value != null && value != "") {
            pageNumber++
            interactor.searchCinemaByKeyword(value, pageNumber)
        }
    }

    fun setGenreCheckBox(genreCheckBox: GenreCheckBox) {
        Log.d(tag, "setGenreCheckBox")
        if (genreCheckBox.genreName != currentGenreCheckBox.genreName) {
            dataClearListener.dataClear()
            pageNumber = 0
            currentGenreCheckBox = genreCheckBox
        }

        if (currentGenreCheckBox.check) {
            if (currentGenreCheckBox.genreName == "Action ") {
                currentGenreValue = "28-action"
            }
            if (currentGenreCheckBox.genreName == "Adventure ") {
                currentGenreValue = "12-adventure"
            }
            if (currentGenreCheckBox.genreName == "Animation ") {
                currentGenreValue = "16-animation"
            }
            if (currentGenreCheckBox.genreName == "Comedy ") {
                currentGenreValue = "35-comedy"
            }
            if (currentGenreCheckBox.genreName == "Crime ") {
                currentGenreValue = "80-crime"
            }
            if (currentGenreCheckBox.genreName == "Documentary ") {
                currentGenreValue = "99-documentary"
            }
            if (currentGenreCheckBox.genreName == "Drama ") {
                currentGenreValue = "18-drama"
            }
            if (currentGenreCheckBox.genreName == "Family ") {
                currentGenreValue = "10751-family"
            }
            if (currentGenreCheckBox.genreName == "Fantasy ") {
                currentGenreValue = "14-fantasy"
            }
            if (currentGenreCheckBox.genreName == "History ") {
                currentGenreValue = "36-history"
            }
            if (currentGenreCheckBox.genreName == "Horror ") {
                currentGenreValue = "27-horror"
            }
            if (currentGenreCheckBox.genreName == "Music ") {
                currentGenreValue = "10402-music"
            }
            if (currentGenreCheckBox.genreName == "Mystery ") {
                currentGenreValue = "9648-mystery"
            }
            if (currentGenreCheckBox.genreName == "Romance ") {
                currentGenreValue = "10749-romance"
            }
            if (currentGenreCheckBox.genreName == "Science Fiction ") {
                currentGenreValue = "878-science_fiction"
            }
            if (currentGenreCheckBox.genreName == "TV Movie ") {
                currentGenreValue = "10770-tv_movie"
            }
            if (currentGenreCheckBox.genreName == "Thriller ") {
                currentGenreValue = "53-thriller"
            }
            if (currentGenreCheckBox.genreName == "War ") {
                currentGenreValue = "10752-war"
            }
            if (currentGenreCheckBox.genreName == "Western ") {
                currentGenreValue = "37-western"
            }
            isLoading.set(true)

            searchCinemaByGenre(currentGenreValue)
        }
    }

    private fun searchCinemaByGenre(value: String) {
        Log.d(tag, "genre search")
        pageNumber++
        interactor.searchCinemaByGenre(value, pageNumber)
    }

    fun getSearchViewModelData(): MutableLiveData<List<MovieModel>> {
        //repository.getDataFromServer()
        return currentData
    }

    private fun setupData() {
        interactor
            .dataFromServerBehaviorSubject
            .subscribeBy(
                onNext = {
                    Log.d("SearchViewModel", "Got new data from Server, emmit search data")
                    currentData.value = it
                }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        interactor.onCleared()
        compositeDisposable.clear()
        super.onCleared()
    }

    interface WhenNeedDataClearListener {
        fun dataClear()
    }

    interface GenreViewHolderEditTextSearchListener {
        fun onGenreEditTextSearchClick()
    }
}