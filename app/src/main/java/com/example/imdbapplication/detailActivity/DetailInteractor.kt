package com.example.imdbapplication.detailActivity

import androidx.lifecycle.MutableLiveData
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import org.koin.core.KoinComponent
import org.koin.core.inject

interface DetailInteractor {
    fun getVideos(id: Int)
    fun getReviews(id: Int)
    fun getImages(id: Int)
    fun addItemInDatabase()
    fun removeItemFromDatabase()
    fun getCurrentVideoData(): MutableLiveData<ArrayList<MovieVideoModel.Result>>
    fun getCurrentReviewData(): MutableLiveData<ArrayList<MovieReviewModel.Result>>
    fun getCurrentImagesData(): MutableLiveData<ArrayList<MovieImagesModel.Backdrop>>
    fun onCleared()
}

class DetailInteractorImpl : DetailInteractor, KoinComponent {

    private val repository: DetailRepository by inject()

    override fun getVideos(id: Int) {
        repository.getVideos(id)
    }

    override fun getReviews(id: Int) {
        repository.getReviews(id)
    }

    override fun getImages(id: Int) {
        repository.getImages(id)
    }

    override fun addItemInDatabase() {
        repository.addItemInDatabase()
    }

    override fun removeItemFromDatabase() {
        repository.removeItemFromDatabase()
    }

    override fun getCurrentVideoData(): MutableLiveData<ArrayList<MovieVideoModel.Result>> {
        return repository.getCurrentVideoData()
    }

    override fun getCurrentReviewData(): MutableLiveData<ArrayList<MovieReviewModel.Result>> {
        return repository.getCurrentReviewData()
    }

    override fun getCurrentImagesData(): MutableLiveData<ArrayList<MovieImagesModel.Backdrop>> {
        return repository.getCurrentImagesData()
    }

    override fun onCleared() {
        repository.onCleared()
    }

}