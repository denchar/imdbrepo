package com.example.imdbapplication.Values

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import io.reactivex.subjects.BehaviorSubject

class Values {
    companion object {
        const val API_KEY = "35ffe1c2c8248e38e4cbb1a187bee8e8"
        const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/"
        const val RECOMENDED_IMAGE_SIZE = "w185"
        const val DEFAULT_ID = 490132

        @SuppressLint("StaticFieldLeak")
        lateinit var cont: Context
        lateinit var mSharedpreferences: SharedPreferences
        fun initialize(context: Context) {
            mSharedpreferences = context.getSharedPreferences(
                context.packageName,
                Context.MODE_PRIVATE
            )
            cont = context
        }

        lateinit var lifecycleOwner: LifecycleOwner
        fun initLifecycleOwner(life: LifecycleOwner) {
            lifecycleOwner = life
        }

        var idListFromDatabaseM: BehaviorSubject<List<String>> = BehaviorSubject.create()
        var idListFromDatabase = MutableLiveData<List<String>>()
        fun getGenres(genreIds: ArrayList<Int>): String {
            var genresList = ""
            for (item in genreIds) {
                when (item) {
                    28 -> genresList += "Action "
                    12 -> genresList += "Adventure "
                    16 -> genresList += "Animation "
                    35 -> genresList += "Comedy "
                    80 -> genresList += "Crime "
                    99 -> genresList += "Documentary "
                    18 -> genresList += "Drama "
                    10751 -> genresList += "Family "
                    14 -> genresList += "Fantasy "
                    36 -> genresList += "History "
                    27 -> genresList += "Horror "
                    10402 -> genresList += "Music "
                    9648 -> genresList += "Mystery "
                    10749 -> genresList += "Romance "
                    878 -> genresList += "Science Fiction "
                    10770 -> genresList += "TV Movie "
                    53 -> genresList += "Thriller "
                    10752 -> genresList += "War "
                    37 -> genresList += "Western "
                }
            }
            return genresList
        }
    }
}