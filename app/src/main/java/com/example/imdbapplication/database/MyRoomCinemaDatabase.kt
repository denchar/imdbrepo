package com.example.imdbapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


@Database(entities = [MyCinemaModel::class, MyCinemaGenresModel::class], version = 4)
abstract class MyRoomCinemaDatabase : RoomDatabase() {
    abstract fun myDao(): MyCinemaDao


    companion object {
        const val DB_NAME = "myappdatabase.db"
        @Volatile
        private var INSTANCE: MyRoomCinemaDatabase? = null

        fun getInstance(context: Context): MyRoomCinemaDatabase? {
            if (INSTANCE == null) {
                synchronized(MyRoomCinemaDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        MyRoomCinemaDatabase::class.java, this.DB_NAME
                    )
                        .addMigrations(MIGRATION_3_4)
                        .fallbackToDestructiveMigration() //убрать
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        private val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE mycinematable ADD COLUMN overview TEXT DEFAULT 0 NOT NULL")
            }
        }
    }


}