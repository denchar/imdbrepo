package com.example.imdbapplication.searchActivity

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_layout.view.*

class SearchRecyclerViewAdapter : RecyclerView.Adapter<SearchRecyclerViewAdapter.ViewHolder>() {
    private val tag = "RecyclerViewAdapter"
    private var data = ArrayList<MovieModel>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener
    private lateinit var onLoadMoreListener: OnLoadMoreCinemaInAdapterListener
    private lateinit var isLoading: ObservableField<Boolean>


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie_layout, parent, false),
            viewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        Log.d(tag, "return data.size = ${data.size}")
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = data[position].title
        val url = Values.BASE_IMAGE_URL + Values.RECOMENDED_IMAGE_SIZE + data[position].poster_path
        Picasso.get().load(url).placeholder(R.mipmap.ic_launcher).into(holder.poster)
        holder.bind(data[position])

        if (!isLoading.get()!! && holder.adapterPosition == itemCount - 1) {
            Log.d(tag, "LoadMore")
            onLoadMoreListener.onLoadMoreCinemaInAdapter()
        }
    }

    fun addListInData(list: List<MovieModel>) {
        Log.d(tag, "add list in data")
        data.addAll(list)
        Log.d(tag, "add list in data. size = ${data.size}")
        notifyDataSetChanged()
    }

    fun dataClear() {
        Log.d(tag, "data is clear")
        data.clear()
    }

    fun setIsLoading(isLoading: ObservableField<Boolean>) {
        this.isLoading = isLoading
        Log.d(tag, "set isLoading")
    }

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreCinemaInAdapterListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(item: MovieModel)
    }

    interface OnLoadMoreCinemaInAdapterListener {
        fun onLoadMoreCinemaInAdapter()
    }

    class ViewHolder(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var poster: ImageView = view.poster_image_view
        var title: TextView = view.movie_title_text_view
        lateinit var movieModel: MovieModel

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(movieModel)
            }
        }

        fun bind(movieModel: MovieModel) {
            this.movieModel = movieModel
        }
    }
}