package com.example.imdbapplication.mainActivity

import com.example.imdbapplication.movieModels.MovieModel
import io.reactivex.subjects.BehaviorSubject

interface MainRepository {

    val dataFromServer: BehaviorSubject<List<MovieModel>>
    fun getLiveDataRequest(pageNumber: Int)
    fun gettingListFromString(genreIds: String): List<Int>
    fun writingStringFromList(list: List<Int>): String
    fun onCleared()

}