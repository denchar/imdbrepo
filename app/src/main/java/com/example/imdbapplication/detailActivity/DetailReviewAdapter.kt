package com.example.imdbapplication.detailActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.R
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import kotlinx.android.synthetic.main.item_review_layout.view.*
import kotlinx.android.synthetic.main.item_trailer_layout.view.*

class DetailReviewAdapter : RecyclerView.Adapter<DetailReviewAdapter.ViewModel>() {

    var data= ArrayList<MovieReviewModel.Result>()

    fun addListInData(list: ArrayList<MovieReviewModel.Result>) {
        data.addAll(list)
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewModel {
        return ViewModel(LayoutInflater.from(p0.context).inflate(R.layout.item_review_layout, p0, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewModel, p1: Int) {
        p0.author.text = data[p1].author
        p0.content.text = data[p1].content
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view) {
        var author = view.author_text_view!!
        var content = view.content_text_view!!
    }
}