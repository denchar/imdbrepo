package com.example.imdbapplication.detailActivity


import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.room.TypeConverter
import com.example.imdbapplication.CacheClass
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.database.MyCinemaDao
import com.example.imdbapplication.database.MyCinemaModel
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import com.example.imdbapplication.retrofit.ApiClient
import com.example.imdbapplication.retrofit.ApiInterface
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get
import kotlin.collections.ArrayList

class DetailRepositoryImpl() : DetailRepository, KoinComponent {

    private val database: MyCinemaDao = get()
    private var compositeDisposable = CompositeDisposable()
    private var dataVideo = MutableLiveData<ArrayList<MovieVideoModel.Result>>()
    private var dataReview = MutableLiveData<ArrayList<MovieReviewModel.Result>>()
    private var dataImages = MutableLiveData<ArrayList<MovieImagesModel.Backdrop>>()
    private val tag = "DetailRepository"

    override fun getCurrentVideoData() = dataVideo
    override fun getCurrentReviewData() = dataReview
    override fun getCurrentImagesData() = dataImages

    override fun getVideos(id: Int) {
        val api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)
        compositeDisposable.add(
            api.getMovieVideos(id, Values.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { addResultInVideoArray(it) },
                    {
                        it.printStackTrace()
                        sendErrVideos()
                    },
                    { println(" Video is Done!") }
                )
        )
    }

    private fun addResultInVideoArray(result: MovieVideoModel) {
        dataVideo.value = result.results
    }

    private fun sendErrVideos() {}

    override fun getReviews(id: Int) {
        val api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)
        compositeDisposable.add(
            api.getMovieReviews(id, Values.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { addResultInReviewArray(it) },
                    {
                        it.printStackTrace()
                        sendErrReviews()
                    },
                    { println(" Review is Done!") }
                )
        )
    }

    private fun addResultInReviewArray(result: MovieReviewModel) {
        dataReview.value = result.results
    }

    private fun sendErrReviews() {}

    override fun getImages(id: Int) {
        val api = ApiClient.getApiRetrofitClient()!!.create(ApiInterface::class.java)
        compositeDisposable.add(
            api.getMovieImages(id, Values.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { addResultInImagesArray(it) },
                    {
                        it.printStackTrace()
                        sendErrImages()
                    },
                    { println(" Images is Done!") }
                )
        )
    }

    private fun addResultInImagesArray(review: MovieImagesModel) {
        val list = ArrayList<MovieImagesModel.Backdrop>()
        for (item in review.backdrops) {
            Log.d(tag, item.file_path)
            list.add(item)
        }
        dataImages.value = list
    }

    private fun sendErrImages() {}

    @SuppressLint("CheckResult")
    override fun addItemInDatabase() {
        Observable.just(CacheClass.movieModel)
            .subscribeOn(Schedulers.io())
            .subscribe {
                database.addCinema(
                    MyCinemaModel(
                        it.id,
                        it.title,
                        it.poster_path,
                        it.overview,
                        it.release_date,
                        writingStringFromList(it.genre_ids)
                    )
                )
            }
    }

    override fun removeItemFromDatabase() {
        Observable.just("")
            .map { database.deleteCinemaById(CacheClass.movieModel.id) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    @TypeConverter
    fun writingStringFromList(list: List<Int>): String {
        var genreIds = ""
        for (i in list) {
            genreIds += ",$i"
        }
        return genreIds
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}