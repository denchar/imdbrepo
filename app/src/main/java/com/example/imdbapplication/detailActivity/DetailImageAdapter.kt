package com.example.imdbapplication.detailActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.movieModels.MovieImagesModel
import com.example.imdbapplication.movieModels.MovieReviewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_image_layout.view.*
import kotlinx.android.synthetic.main.item_review_layout.view.*

class DetailImageAdapter : RecyclerView.Adapter<DetailImageAdapter.ViewHolder>() {

    var data = ArrayList<MovieImagesModel.Backdrop>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_image_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val url = Values.BASE_IMAGE_URL + Values.RECOMENDED_IMAGE_SIZE + data[position].file_path
        Picasso.get().load(url).into(holder.image)

    }

    fun addListInData(list: ArrayList<MovieImagesModel.Backdrop>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var image: ImageView = view.item_image

    }

}