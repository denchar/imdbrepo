package com.example.imdbapplication.mainActivity


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.CacheClass
import com.example.imdbapplication.ConnectionService
import com.example.imdbapplication.R
import com.example.imdbapplication.databinding.ActivityMainBinding
import com.example.imdbapplication.detailActivity.DetailActivity
import com.example.imdbapplication.localDBActivity.LocalDbActivity
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.searchActivity.SearchActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), MainRecyclerViewAdapter.ViewHolderClickListener,
    MainRecyclerViewAdapter.OnLoadMoreCinemaInAdapterListener {

    val tag = "MainActivity"
    private val adapter: MainRecyclerViewAdapter by inject()
    private lateinit var activityMainBinding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModel()
    private val connectionService: ConnectionService by inject()
    private lateinit var itemTouchHelper: ItemTouchHelper
    private var haveConnection = ObservableField<Boolean>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        haveConnection = connectionService.getConnectStatus()

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.viewModel = mainViewModel
        mainViewModel.haveConnection = haveConnection
        activityMainBinding.executePendingBindings()
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Search Activity", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show()
            SearchActivity.startSearchActivity(this)
        }

        fab_local_database.setOnClickListener {
                view ->
            Snackbar.make(view, "Local Database Activity", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show()
            LocalDbActivity.startLocalDBActivity(this)
        }
        startMyActivity()
    }

    private fun startMyActivity() {
        adapter.setListener(this)
        adapter.setOnLoadMoreListener(this)
        adapter.setIsLoading(mainViewModel.isLoading)
        movie_recycler_view.adapter = adapter

        itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val position = viewHolder.adapterPosition
                val name = adapter.getItemViewPosition(position).title
                if (ItemTouchHelper.LEFT == direction) {
                    Log.d(tag, "$name left")
                }
                if (ItemTouchHelper.RIGHT == direction) {
                    Log.d(tag, "$name right")
                }

                // add to DB
            }
        })
        itemTouchHelper.attachToRecyclerView(movie_recycler_view)

        addListInAdapter()
    }

    override fun onViewHolderClick(item: MovieModel) {
        CacheClass.movieModel = item
        DetailActivity.startMovieDetailActivity(this)
    }

    override fun onLoadMoreCinemaInAdapter() {
        mainViewModel.loadData()
    }

    private fun addListInAdapter() {
        mainViewModel.getMainViewModelData().observe(this, Observer<List<MovieModel>> { list ->
            adapter.addListInData(list!!)
            mainViewModel.isLoading.set(false)
            //list.forEach { println("from main : " + it.title) }
        })
    }

}
