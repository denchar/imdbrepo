package com.example.imdbapplication.localDBActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.R
import com.example.imdbapplication.Values.Values
import com.example.imdbapplication.movieModels.MovieModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_layout.view.*

class LocalDBAdapter : RecyclerView.Adapter<LocalDBAdapter.ViewHolder>() {
    private var data = ArrayList<MovieModel>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    fun addListInData(list: List<MovieModel>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    fun getItemViewPosition(position: Int): MovieModel {
        return data[position]
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.item_view_localdb, p0, false),
            viewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.title.text = data[p1].title
        val url = Values.BASE_IMAGE_URL + Values.RECOMENDED_IMAGE_SIZE + data[p1].poster_path
        Picasso.get().load(url).into(p0.poster)
        p0.bind(data[p1])

    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(item: MovieModel)
    }


    class ViewHolder(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var poster: ImageView = view.poster_image_view
        var title: TextView = view.movie_title_text_view
        lateinit var movieModel: MovieModel

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(movieModel)
            }
        }

        fun bind(movieModel: MovieModel) {
            this.movieModel = movieModel
        }

    }
}