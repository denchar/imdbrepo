package com.example.imdbapplication.detailActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbapplication.R
import com.example.imdbapplication.mainActivity.MainRecyclerViewAdapter
import com.example.imdbapplication.movieModels.MovieModel
import com.example.imdbapplication.movieModels.MovieVideoModel
import kotlinx.android.synthetic.main.item_trailer_layout.view.*

class DetailTrailerAdapter :   RecyclerView.Adapter<DetailTrailerAdapter.ViewModel>() {

    private var data = ArrayList<MovieVideoModel.Result>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener

    fun addListInData(list: ArrayList<MovieVideoModel.Result>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewModel {
        return ViewModel(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_trailer_layout,
                p0, false
            ), viewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewModel, p1: Int) {
        p0.trailerName.text = data[p1].name
        p0.bind(data[p1])
    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(result: MovieVideoModel.Result)
    }

    class ViewModel(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {

        val trailerName = view.trailer_name_text_view!!
        lateinit var result: MovieVideoModel.Result

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(result)
            }
        }

        fun bind(result: MovieVideoModel.Result) {
            this.result = result
        }
    }
}